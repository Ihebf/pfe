package com.example.demo.controller;

import com.example.demo.dto.CreatePatientDto;
import com.example.demo.entites.Alarm;
import com.example.demo.entites.Measure;
import com.example.demo.entites.Patient;
import com.example.demo.entites.User;
import com.example.demo.repositories.AlarmRepository;
import com.example.demo.repositories.MeasureRepository;
import com.example.demo.repositories.PatientRepository;
import com.example.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PatientController {

    @Autowired
    PatientRepository patientRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AlarmRepository alarmRepository;
    @Autowired
    MeasureRepository measureRepository;

    private static final int TEMPERATURE_NOMINAL_MAX= 37;
    private static final int TEMPERATURE_NOMINAL_MIN= 35;
    private static final int BMP_NOMINAL_MAX= 80;
    private static final int BMP_NOMINAL_MIN= 40;

    private static final String BMP_ABOVE_NOMINAL="BMP dessus la nominal";
    private static final String BMP_BELOW_NOMINAL="BMP dessous la nominal";
    private static final String TEMPERATURE_EXCEEDS_NOMINAL="température dépasse la nominal";
    private static final String TEMPERATURE_LESS_THEN_NOMINAL="température inférieure à la nominal";

    private static final String DATE_PATTERN = "yyyy-mm-dd hh:mm:ss";

    @GetMapping("/patients/{id}")
    public ResponseEntity<List<Patient>> getUserPatients(@PathVariable int id){
        try {
            Optional<User> userData = userRepository.findById(id);
            if(userData.isEmpty()){
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }else {
                User user = userData.get();
                return new ResponseEntity<>(user.getPatientList(),HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/patient")
    public ResponseEntity<Patient> getPatient(@RequestParam int id){
        try {
            List<Patient> patients = getUserPatients(id).getBody();
            assert patients != null;
            if(!patients.isEmpty()){
                for (Patient patient : patients){
                    if(patient.getId()==(id)){
                        return new ResponseEntity<>(patient,HttpStatus.OK);
                    }
                }
            }
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        }catch (Exception e){
            return new ResponseEntity<>(null,getUserPatients(id).getStatusCode());
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/create_patient")
    public ResponseEntity<Patient> createPatient(@RequestBody CreatePatientDto createPatientDto){
        try {
            User userData = userRepository.findById(createPatientDto.getUserId()).get();
            Patient _patient = new Patient();
            _patient.setFirstName(createPatientDto.getFirstName());
            _patient.setLastName(createPatientDto.getLastName());
            _patient.setRoomNum(createPatientDto.getRoomNum());

            Measure measure = randomMeasure();
            Alarm alarm = alarmFlag(measure);

            _patient.setMeasure(measure);
            _patient.setUser(userData);
            _patient.setAlarm(alarm);

            patientRepository.save(_patient);
            patientRepository.flush();

            List<Patient> patientList = userData.getPatientList();
            patientList.add(_patient);

            userData.setPatientList(patientList);
            userRepository.save(userData);

            measureRepository.save(measure);
            if(alarm!=null)
                alarmRepository.save(alarm);

            return new ResponseEntity<>(_patient, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/patient/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") int id){
        try {

            patientRepository.deleteById(id);
            patientRepository.flush();
            return new ResponseEntity<>("patient deleted",HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Failed to delete patient",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private Measure randomMeasure() {
        Measure measure = new Measure();
        measure.setTemperature(random(36,45));
        measure.setBpm(Math.toIntExact(random(30, 100)));
        measure.setDate(getDate());

        return measure;
    }

    private Long random(int low,int high) {
        Random r = new Random();
        int result = r.nextInt(high-low) + low;
        return (long) result;
    }

    private Alarm alarmFlag(Measure measure) {
        Alarm alarm = new Alarm();
        Long t = measure.getTemperature();
        Integer bpm = measure.getBpm();
        if(t.intValue() > TEMPERATURE_NOMINAL_MAX)
            alarm.setCause(TEMPERATURE_EXCEEDS_NOMINAL);
        if(bpm > BMP_NOMINAL_MAX)
            alarm.setCause(BMP_ABOVE_NOMINAL);
        if(bpm < BMP_NOMINAL_MIN)
            alarm.setCause(BMP_BELOW_NOMINAL);
        if(t.intValue() >= TEMPERATURE_NOMINAL_MAX && bpm >= BMP_NOMINAL_MAX)
            alarm.setCause(TEMPERATURE_EXCEEDS_NOMINAL+","+BMP_ABOVE_NOMINAL);
        if(t.intValue() >= TEMPERATURE_NOMINAL_MAX && bpm <= BMP_NOMINAL_MIN)
            alarm.setCause(TEMPERATURE_EXCEEDS_NOMINAL+","+BMP_BELOW_NOMINAL);

        if(alarm.getCause() == null)
            return null;

        alarm.setDate(getDate());

        return alarm;
    }

    private String getDate(){
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        return dateFormat.format(date);
    }
}
