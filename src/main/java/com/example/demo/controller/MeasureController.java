package com.example.demo.controller;

import com.example.demo.entites.Patient;
import com.example.demo.entites.User;
import com.example.demo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class MeasureController {

    @Autowired
    PatientRepository patientRepository;

    @GetMapping("/patient/{id}/measure")
    public ResponseEntity<?> getPatientsAlarm(@PathVariable int id){
        try {
            Optional<Patient> patientData = patientRepository.findById(id);
            if(patientData.isEmpty()){
                return new ResponseEntity<>("patient not found", HttpStatus.NOT_FOUND);
            }else {
                Patient patient = patientData.get();
                if(patient.getMeasure()==null){
                    return new ResponseEntity<>("patient don't have an alarm",HttpStatus.NOT_FOUND);
                }
                return new ResponseEntity<>(patient.getMeasure(),HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
