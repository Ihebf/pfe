package com.example.demo.controller;

import com.example.demo.dto.SignInDto;
import com.example.demo.dto.SignUpDto;
import com.example.demo.entites.User;
import com.example.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserRepository userRepository;

    private static final String DOCTOR_ROLE_KEY= "doctor";
    private static final String NURSE_ROLE_KEY= "nurse";

    @RequestMapping(method = RequestMethod.POST, value = "/sign_in")
    public ResponseEntity<User> signIn(@RequestBody SignInDto signInDto) {
        String email = signInDto.getEmail();
        String password = signInDto.getPassword();
        if (userRepository.existsByEmail(email)) {
            User user = userRepository.findByEmail(email).get();
            if (user.getPassword().equals(password)) {
                user.setAuth(true);
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sign_up")
    public ResponseEntity<?> signUp(@RequestBody SignUpDto signUpDto){
        if(userRepository.existsByEmail(signUpDto.getEmail())){
            return new ResponseEntity<>("Email is already taken!",HttpStatus.BAD_REQUEST);
        }
        User _user = new User();
        _user.setEmail(signUpDto.getEmail());
        _user.setPassword(signUpDto.getPassword());
        _user.setFirstName(signUpDto.getFirstName());
        _user.setLastName(signUpDto.getLastName());
        _user.setRole(signUpDto.getRole());
        _user.setSpeciality(signUpDto.getSpeciality());
        _user.setAuth(false);
        userRepository.save(_user);
        return new ResponseEntity<>(_user, HttpStatus.OK);
    }










}
