package com.example.demo.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "mesure")
public class Measure {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "temperature") // < 40 : température dépasse la nominal
    private Long temperature;
    @Column(name = "bpm") // < 80 > 50 : BMP dessus la nominal / BMP dessous la nominal
    private Integer bpm;
    @Column(name = "date")
    private String date;


}
