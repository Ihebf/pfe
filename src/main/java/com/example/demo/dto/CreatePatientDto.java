package com.example.demo.dto;

import lombok.Data;

@Data
public class CreatePatientDto {
    private String firstName;
    private String lastName;
    private String roomNum;
    private int userId;
}
