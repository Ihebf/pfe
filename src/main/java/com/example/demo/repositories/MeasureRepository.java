package com.example.demo.repositories;

import com.example.demo.entites.Measure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MeasureRepository extends JpaRepository<Measure, Integer> {

}
