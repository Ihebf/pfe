package com.example.demo.repositories;

import com.example.demo.entites.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

   // boolean existsByFirstNameAndLastNameAndBirthday(String firstName,String LastName,String birthday);
}
