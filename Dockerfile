FROM adoptopenjdk/openjdk11:alpine-jre
EXPOSE 8090
WORKDIR /opt/app
ARG JAR_FILE="target/demo-0.0.1-SNAPSHOT.jar"
COPY ${JAR_FILE} pfe.jar
ENTRYPOINT ["java","-jar","pfe.jar"]
